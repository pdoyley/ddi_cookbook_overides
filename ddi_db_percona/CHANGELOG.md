# CHANGELOG for ddi_db_percona

This file is used to list changes made in each version of ddi_db_percona.

## 0.1.0:

* Initial release of ddi_db_percona

- - -
Check the [Markdown Syntax Guide](http://daringfireball.net/projects/markdown/syntax) for help with Markdown.

The [Github Flavored Markdown page](http://github.github.com/github-flavored-markdown/) describes the differences between markdown on github and standard markdown.
